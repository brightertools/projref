# --------------------------------------------------------------------------------------
# Gets a distinct list of project references from all .csproj files within the rootPath
# For more info see: https://projref.codeplex.com/ 
# © 2014 Brighter Tools Ltd
# --------------------------------------------------------------------------------------
param
(
	[Parameter(Mandatory=$True)]
	[string]$rootPath,
	[string]$outputFile
)

if ([string]::IsNullOrEmpty($outputFile))
{
	$outputFile = "ProjRef.log"
}

if (Test-Path $outputFile){
	Remove-Item $outputFile
}

# Logs to file and console
function log($string, $color)
{
   if ($Color -eq $null) {$color = "gray"}
   write-host $string -foregroundcolor $color
   $string | out-file -filepath $outputFile -append
}

# Gets paths to all project files
function GetProjectFiles($path)
{
    $pathProjects = @(dir -path $path -filter *.csproj)
	
	foreach ($i in $pathProjects) { 
		$i | select FullName 
	}
	
	$subFolders = @(dir -path $path  -attributes !ReparsePoint | where { $_.Attributes -eq 'Directory' -and $_.Name.ToString().StartsWith(".") -eq $false -and $_.Name.ToString().StartsWith("_") -eq $false} )
	
	foreach ($subFolder in $subFolders) { 
		GetProjectFiles($subFolder.FullName) 
	}
}

CD $rootPath

clear-host

write-host "ProjRef"
log ""
log "Generating project reference list..."
log ""
write-host ("PowerShell version: " + $PSVersionTable.PSVersion)
write-host ""
log "Root Path : $rootPath" 
log ""

write-host "Searching folders..."

$projectFiles = @(GetProjectFiles($rootPath))

$references = @()

if ($projectFiles.length -eq 0) {

	log "No projects found" yellow
	
} else {

	log ($projectFiles.length.ToString() + " projects found")
	
	foreach($project in $projectFiles)
	{	
		[XML]$projectXML = get-content $project.FullName
		$nodes = $projectXML.SelectNodes('//*[local-name()="HintPath"]')
		foreach($node in $nodes) 
		{
			$referenceFolder = split-path $node.InnerText -Parent
			
			if (!($references -contains $referenceFolder))
			{
				$references += $referenceFolder
			}
		}
	}
	
	log ""
	log ($references.length.ToString() + " distinct references found")
	log ""
	
	foreach($reference in $References) 
	{
		log $reference
	}
}

log ""
log "Complete" green
log ""
log (get-date -format G)
log ""

start notepad $outputFile