Version 2.0.0
-------------
UPDATED: Folder search to exclude symbolic link / ReparsePoint and folders and folder names starting with "." or "_"
UPDATED: Outputs to console and log file


Version 1.0.0
-------------
Initial Release